"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var dotenv = require("dotenv");
var path = require("path");
dotenv.config({ path: path.resolve(__dirname, '..', 'config', 'cfg.env') });
var app = express();
app.listen(process.env.PORT, function () {
    console.log("listening on ".concat(process.env.PORT));
});
