import * as express from 'express'; 
import * as dotenv from 'dotenv';
import * as path from "path";

dotenv.config({path: path.resolve(__dirname, '../config/cfg.env')});

const app = express(); 

app.listen(process.env.PORT, () => {
    console.log(`listening on ${process.env.PORT}`);
})